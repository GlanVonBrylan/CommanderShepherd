"use strict";

const Discord = require("discord.js");
const INTENTS = Discord.GatewayIntentBits;
const auth = require("./auth.json");
const { token, master: masterId } = auth;

const client = new Discord.Client({
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildMessages, INTENTS.MessageContent,
		INTENTS.DirectMessages,
	],
	partials: [Discord.Partials.Channel], // for DMs
});

const error = require("./utils/error");

var master;
const cleanup = [], onMasterReady = [];

module.exports = exports = {
	client,
	auth,
	checkMaster: u => u?.id === masterId,
	onMasterReady: func => {
		if(typeof func !== "function")
			throw new TypeError("The argument must be a function.");
		return onMasterReady.push(func);
	},

	delayedMsgDeletion: msg => setTimeout(() => msg.delete().catch(Function()), 5000),

	registerCleanup: func => {
		if(typeof func !== "function")
			throw new TypeError("The argument must be a function.");
		return cleanup.push(func);
	},
	exit: () => {
		master.send("Shutting down... please wait for a few seconds.");
		for(const func of cleanup)
			func();
		setTimeout(() => { client.destroy(); process.exit(); }, 3000); // To make sure all lambs are shut down.
	}
}


client.on("error", error);

client.on("ready", () => {
	console.log(`Logged in as ${client.user.tag}!`);
	client.users.fetch(masterId).then(usr => {
		exports.master = master = usr;
		for(const func of onMasterReady)
			func(usr);
	});
});


client.login(token);
