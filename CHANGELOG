0.1 (04/09/2018)
The Shepherd starts the other bot, which die when the Shepherd does.

0.2 (04/09/2018)
The Shepherd can now efficiently kill its children (what a monster).

0.3 (04/09/2018)
The Shepherd can now tell the master which bots are running. Also, all commands related to bots have been effectively reserved to the master.



1.0 (04/09/2018)
Updating bots now work (yay!).
Also, we can remotely shut down the Shepherd.


1.1 (04/09/2018)
Added the crashForgiveness, a function executes every 10 minutes that decrements the restartTries of everyone bot.
Also, updating a bot resets its restartTries to 0.

	1.1.1 (07/09/2018)
	Fix the Shepherd trying again to start the bots when a connection error made it reconnect and fire the "ready" event again.

	1.1.2 (13/09/2018)
	Fixed the Shepherd thinking the update failed while it did not. Why does git send some info through stderr though?..


1.2 (14/09/2018)
Added a |status command to check how a bot is doing.
Also, |runningBots is not public.

	1.2.1 (22/09/2018)
	The |status command now behaves like |runningBot if no argument is provided.

	1.2.2 (26/09/2018)
	Fixed a crash when sending it an empty message (for instance with an embedded file).


1.3 (07/01/2019)
In the "unknown command" message, replaced "$help" with "|help".
Fixed the |status command that would no udpate the bot version after updating a bot.
Added a |version command.

	1.3.1 (16/01/2019)
	Commander Shepherd now updates the bots on startup.

	1.3.2 (01/02/2019)
	Commander Shepherd does not say "unknown command" to spoiler tags anymore.

	1.3.3 (08/02/2019)
	Commander Shepherd now reacts with a check mark ✅ when successfully changing a parameter.
	When changing the fully charged warning interval, the command is no longer needlessy executed. Also fixed the local variable "error" shadowing the eponymous global function.


1.4 (18/04/2019)
Added the |restart command, that restarts (and updates) the Shepherd and all child bots.
The command for the Battery Warden is now in auth.json instead of being hard-coded in bot.js.


1.5 (25/07/2019)
Commander Shepherd now logs child bot errors into files.
These logs can be managed remotely with the |logs commands.
Adds an essential |ping command.

	1.5.1 (14/08/2019)
	Fixed retrieving logs.

	1.5.2 (26/08/2019)
	Fixed retrieving a specific log.
	Added the possibility to get all the logs of a bot.

	1.5.3 (29/08/2019)
	Retrieving all logs of a bot is now a bit faster, and should not cause a huge lag anymore if there are a lot.


1.6 (04/09/2019)
Adds the |update-force and |exec commands.
The |eval command now catches exceptions, and sends the result back in case of success.


1.7 (09/02/2020)
Adds the |uptime command.


1.8 (01/03/2020)
Adds the |updateBot-pkg command.


1.9.0 (20/03/2020)
Now uses Discord.js v12.
Removes [updateBot-pkg.
|updateBot now executes npm-ci.


1.10.0 (04/09/2020)
Adds Intents.



2.0 (18/10/2020)
Complete rework of the codebase.

	2.0.1 (23/10/2020)
	Fixes the help for the |logs commands.
	Fixes |logs all.


2.1 (26/12/2020)
|update and |update-force don't execute npm ci by default anymore, and instead need the -ci option.
Adds a -ci option to |restart.

	2.1.1 (28/03/2021)
	|exec now puts the output in monospace.


2.2 (06/06/2021)
Adds a -dl option to |logs all, which allows downloading the logs as a zip.
Minor bug fixes and improvements.

	2.2.1 (05/10/2021)
	Adds proper feedback to |exec when the command succeeds without any output.


2.3 (12/01/2022)
Replaces |udateLamb-force with a -f option on |updateLamb.
Adds |addLamb, |placeFile and |removeLamb.
Fixes |restart

	2.3.1 (02/11/2022)
	|updateLamb, |stopLamb etc now try to remove the spaces from the name if the lamb can't be found.

	2.3.2 (13/04/2022)
	The "unknown command" message is now deleted after 2 seconds.

	2.3.3 (31/05/2023)
	Prevents a crash when trying to sent a log that doesn't exist.

	2.3.4 (09/08/2023)
	Saved JSON files are now indented.

	2.3.5 (21/08/2023)
	|log commands now try to remove spaces, like lamb commands.


2.4 (09/09/2022)
Can now use other engines than NodeJS.

2.5 (18/06/2024)
Can now list how many logs each lamb has with |logs list

2.6 (21/07/2024)
Removes the long-unused battery warden.