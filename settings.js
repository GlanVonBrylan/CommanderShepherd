"use strict";

const fs = require("node:fs");

const version = 1;
const defaultSettings = {
	state: "normal",
	passPullInfo: true,
};
var settings, saving = false;

if(fs.existsSync("settings.json"))
{
	const readFile = file => JSON.parse(fs.readFileSync(file, "utf-8"));
	try {
		settings = readFile("settings.json");
	}
	catch(err) {
		console.error(err);
		settings = readFile("settings.temp.json");
	}
}

settings = { ...defaultSettings, ...settings, version };
module.exports = settings;

Object.defineProperty(settings, "save", {value: () => {
	if(saving)
		setTimeout(settings.save, 300);
	else
	{
		saving = true;
		fs.writeFile("settings.temp.json", JSON.stringify(settings, null, "\t"), err => {
			if(err)
				console.error(err);
			else
				fs.createReadStream("settings.temp.json").pipe(fs.createWriteStream("settings.json"))
				.on("close", () => saving = false);
		});
	}
}});
