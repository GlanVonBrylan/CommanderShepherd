"use strict";

const { checkMaster, onMasterReady } = require("../discord");

module.exports = exports = (command, msg, params) => {
	const trueName = command.toLowerCase();
	command = cmds[trueName];
	if(typeof command !== "function")
		return null;
	return typeof command === "function"
		? (command.masterRestricted && !checkMaster(msg.author) ? false : command(msg, params, trueName))
		: null;
}

exports.register = register;


const cmds = {};
const { Collection } = require("discord.js");
const cmdsInfos = new Collection();
const masterCmdsInfos = new Collection();

/**
 * Register the given command.
 * @param {string|Array<string>} names The names/aliases of the command.
 * @param {string} [params] The parameters of the command. This can be ommitted.
 * @param {string} description The description of the command.
 * @param {function} command The command that will be executed. It should take a Message as its first argument, and an array of strings as its second (the parameters). It must not return null. It should return false if a permission was missing to execute it.
 * @param {object} [options] Whether this is an master command.
   * @param {boolean} [options.masterOnly] Whether this is an master-only command.
   * @param {boolean} [options.inline] Whether to put this command's description inline.
 *
 * @throws {TypeError} if 'names' is empty, or not a string or array of non-empty strings, or if 'command' is not a function.
 */
function register(names, params, description, command, {masterOnly, inline} = {masterOnly: false, inline: false}) {
	if(typeof description === "function") // no params?
	{
		masterOnly = command;
		command = description;
		description = params;
		params = null;
	}

	if(!names || names instanceof Array && !names.length)
		throw new TypeError("'names' cannot be empty.");
	if(typeof names === "string")
		names = [names];
	if(!(names instanceof Array) || names.some(n => !n || typeof n !== "string"))
		throw new TypeError("'names' must be a string or array of non-empty strings.");
	if(typeof command !== "function")
		throw new TypeError("'command' should be a function.");

	for(const name of names)
		cmds[name.toLowerCase()] = command;

	const [main, ...aliases] = names;
	command.mainName = main;
	command.masterRestricted = masterOnly;
	(masterOnly ? masterCmdsInfos : cmdsInfos).set(main, { aliases, params, description, inline });
}


register(["help", "aide"],
"Displays the commands.",
({author, channel}) => {
	channel.send(HELP_EMBED); // defined at the end of the file
	if(checkMaster(author))
		channel.send(MASTER_HELP_EMBED);
});


// load all commands
require("node:fs").readdirSync(__dirname).map(f => `./${f}`).forEach(require);


var HELP_EMBED, MASTER_HELP_EMBED;
function cmdInfoToFields({aliases, params, description, inline}, name) {
	return {
		name: `|${name}${params ? " "+params : ""}`,
		value: (aliases.length ? `*Alias : |${aliases.join(", |")}*\n` : "") + description,
		inline,
	};
}
onMasterReady(master => {
	HELP_EMBED = { embeds: [{
		color: 0x1166DD,
		title: "Commands",
		fields: cmdsInfos.map(cmdInfoToFields),
		footer: {
			text: "Made by " + master.username,
			icon_url: master.avatarURL(),
		}
	}]};
	MASTER_HELP_EMBED = { embeds: [{
		color: 0xBBFFBB,
		title: "Commands reserved to you, master",
		fields: masterCmdsInfos.map(cmdInfoToFields),
	}]};
});
