"use strict";

const fs = require("node:fs");

const { register } = require(".");
const error = require("../utils/error");

const settings = require("../settings");
const {
	addLamb, removeLamb,
	placeFile,
	startLamb,
	stopLamb,
	restartLamb,
	updateLamb,
	lambs,
} = require("../shepherd");
const { root } = require("../discord").auth;


const masterOnly = true, inline = true;


function actionOnLamb(func, {channel}, prm, ...options) {
	if(prm.length)
	{
		try { func(prm.join(" "), ...options, channel); }
		catch({message}) { channel.send(message); }
	}
	else
		channel.send("You must provide the lamb's name.");
}


register(["addLamb", "add"], "<git repo> <lamb>",
"Adds the given lamb, cloning its repo and adding it to auth.json.",
(msg, [git, ...lamb]) => actionOnLamb(addLamb, msg, lamb, git),
{masterOnly, inline});

register(["removeLamb", "remove"], "[-rm] <lamb>",
"Removes the given lamb from auth.json, and if `-rm` is specified, deletes its folder.",
(msg, lamb) => {
	let rm = lamb[0] === "-rm";
	if(rm) lamb.shift();
	actionOnLamb(removeLamb, msg, lamb, rm);
}, {masterOnly, inline});

register("placeFile", "<lamb>",
"Places the given file in the lamb's folder. You must join the file alongside the command.\nYou can specify a path after the lamb name, for example `|placeFile My cool app/data/yml`",
(msg, lamb) => {
	if(!msg.attachments.size)
		return msg.channel.send("You must provide a file.");

	actionOnLamb(placeFile, msg, lamb, msg.attachments.first());
},
{masterOnly, inline});


register(["startLamb", "start"], "<lamb>",
"Starts the given lamb. Don't forget all lambs listed in `auth.json` are started when starting the shepherd.",
(msg, lamb) => actionOnLamb(startLamb, msg, lamb),
{masterOnly, inline});

register(["restartLamb"], "<lamb>",
"*Alias: |restart*\nRestarts the given lamb.",
(msg, lamb) => actionOnLamb(restartLamb, msg, lamb),
{masterOnly, inline});

register(["stopLamb", "stop", "end", "kill"], "<lamb>",
"Stops the given lamb.",
(msg, lamb) => actionOnLamb(stopLamb, msg, lamb, false),
{masterOnly, inline});

register(["updateLamb", "update"], "[-ci] [-f] <lamb>",
"Pulls changes from the given lamb's repository, and if successful, restarts it.\nAdd the `-ci` option to also update its packages to match the lockfile, and `-f` to force pull.",
(msg, lamb) => {
	let ci = false, force = false;
	while(["-ci", "-f", "--force"].includes(lamb[0]))
	{
		if(lamb[0] === "-ci") ci = true;
		else force = true;
		lamb.shift();
	}
	actionOnLamb(updateLamb, msg, lamb, force, ci);
},
{masterOnly, inline});


register(["runningLambs", "lambs", "lambsRunning", "running"],
"Tells which lambs are currently running.",
c_runningLambs);
function c_runningLambs({channel}) {
	const running = [];
	for(const [lambName, {process}] of Object.entries(lambs))
		if(process)
			running.push(lambName);

	channel.send(`Currently, ${running.length ? (running.join(", ")+" are") : "none is"} running.`);
}

register(["status", "statut", "version"], "<lamb>",
"Show the current status of the given lamb.",
({channel, mentions}, prm, cmd) => {
	let lamb = mentions.users.first();
	if(lamb)
	{
		if(lamb.bot)
			lamb = lamb.username;
		else
			return channel.send("... This person is not even a bot.");
	}
	else
		lamb = prm.join(" ");

	const versionOnly = cmd === "version";

	if(lamb)
	{
		const bot = lambs[lamb];

		if(bot)
		{
			const lambInfo = JSON.parse(fs.readFileSync(`${root}/${lamb}/package.json`, "utf-8")); // not 'require' because it caches the file!

			if(versionOnly)
				channel.send(`Their current version is ${lambInfo.version}.`);
			else
				channel.send(`${lamb} is currently ${bot.process
					? ["online", "running", "doing fine"].rand()
					: (bot.restartTries === 3
						? ["sick", "bed-ridden", "unable to work"].rand()
						: ["resting a bit", "taking a nap", "on a break"].rand())}.\nTheir current version is ${lambInfo.version}.`
				);
		}
		else
			channel.send("This lamb is not among my sweet children... Are you sure you spelled their name right?");
	}
	else if(versionOnly)
	{
		const versions = Object.keys(lambs).map(lamb => {
			const file = `${root}/${lamb}/package.json`;
			return fs.existsSync(file)
				? `${lamb} version: ${JSON.parse(fs.readFileSync(file)).version}`
				: "";
		});
		channel.send(versions.join("\n"));
	}
	else
		c_runningLambs({channel});
});



register("passPullInfo", "[true|false]",
"If `true` is provided, you will get all output from `git pull`s when updating a lamb; if `false` is, you won't.\nIf you do not provide this parameter, this command tells you whether you get pull info or not.",
({channel}, [prm]) => {
	if(prm)
	{
		if(prm === "true")
			settings.passPullInfo = true;
		else if(prm === "false")
			settings.passPullInfo = false;
		else
		{
			channel.send("Error: `passPullInfo` should be either `true` or `false`.");
			return;
		}

		settings.save();
		msg.react("✅");
	}
	else
		channel.send(`Pull information is currently ${settings.passPullInfo ? "passed to you." : "kept hidden."}`);
}, {masterOnly, inline});
