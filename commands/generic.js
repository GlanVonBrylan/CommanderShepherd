"use strict";

const { client } = require("../discord");
const { register } = require(".");
const splitMessage = require("../utils/splitMessage");

var PRESENTATION;
require("../discord").onMasterReady(master => PRESENTATION = { embeds: [{
	color: 0x1166DD,
	title: "Commander Shepherd",
	description: "This wonderful shepherd takes care of all other lambs, starting them, updating them, and killing them if the master says so.\nFor more information about my features, type `|help`.",
	fields: [
		{ name: "Repository", value: "https://framagit.org/GlanVonBrylan/CommanderShepherd" },
		{ name: "licence", value: "https://www.gnu.org/licenses/gpl-3.0.txt" },
	],
	footer: {
		text: "Made by " + master.username,
		icon_url: master.avatarURL(),
	},
}]});


register(["presentation", "présentation", "description"],
"Introduces this bot.",
({channel}) => channel.send(PRESENTATION));


register("ping",
"Pong!",
({channel}) => channel.send(`Pong! (${client.ws.ping}ms)`));


register("uptime",
"Tells you for how long I have been running.",
({channel}) => {
    let seconds = ~~process.uptime(),
		days    = ~~(seconds / 84400);
	seconds -= days * 84400;
    let hours   = ~~(seconds / 3600);
	seconds -= hours * 3600;
	let minutes = ~~(seconds / 60);
	seconds -= minutes * 60;

    if(hours   < 10) hours   = "0"+hours;
    if(minutes < 10) minutes = "0"+minutes;
    if(seconds < 10) seconds = "0"+seconds;

    channel.send(`${days} day${days > 1 ? "s" : ""}, ${hours}:${minutes}:${seconds}`);
});



register("eval", "<code>",
"Executes the given code. Be careful with that!",
(msg, prm) => {
	let res;
	try {
		res = eval(prm.join(" "));
		if(res === undefined)
			msg.react("✅");
		else
			msg.channel.send("Result: " + res);
	}
	catch(err) {
		require("../discord").master.send(err.toString());
	}
}, {masterOnly: true});


const { exec } = require("node:child_process");

register("exec", "<command>",
"Executes the given POSIX command.",
({channel}, prm) => {
	if(prm.length)
		exec(prm.join(" "), (err, stdout, stderr) => {
			const out = err ? stderr : stdout;
			if(!out)
				channel.send(err?.message || "Command exited with code 0");
			else if(out.length > 10000)
				channel.send(`${err?.message}\nOutput too long (${out.length} bytes)`);
			else for(const chunk of splitMessage(out, {maxLength: 1950}))
				channel.send(`\`\`\`${chunk}\`\`\``);
		});
	else
		channel.send("You must provide a command.");
}, {masterOnly: true});
