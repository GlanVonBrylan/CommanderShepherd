"use strict";

const { splitMessage } = require("../utils/splitMessage");
const error = require("../utils/error");
const fs = require("node:fs");
const { root, lambs } = require("../auth.json");

const JSZip = require("jszip");


if(!fs.existsSync(root + "/logs"))
	fs.mkdirSync(root + "/logs");



function sendLog(channel, folder, file) {
	let log;
	const path = `${folder}/${file}`;
	try {
		log = fs.readFileSync(path, "utf-8");
	} catch {
		return [];
	}

	let res = [];

	try {
		res = splitMessage(`**\n${file}**\n${fs.readFileSync(path, "utf-8")}`)
			.map(channel.send.bind(channel));
	}
	catch { // The split option throws an error if a line is longer than the limit
		channel.send(`**\n${file}**`);
		do {
			let cutPosition = log.substring(0, 1950).lastIndexOf("\n");
			if(cutPosition < 1)
				cutPosition = 1950;

			res.push(channel.send(log.substring(0, cutPosition)));
			log = log.substring(cutPosition + 1).trim();
		}while(log);
	}

	return res;
}

const cmds = {};
const cmdsInfos = new (require("discord.js").Collection);
function register(names, params, description, command)
{
	if(typeof description === "function") // no params?
	{
		command = description;
		description = params;
		params = null;
	}

	if(!names || names instanceof Array && !names.length)
		throw new TypeError("'names' cannot be empty.");
	if(typeof names === "string")
		names = [names];
	if(!(names instanceof Array) || !names.reduce((acc, n) => acc && n && typeof n === "string", true))
		throw new TypeError("'names' must be a string or array of non-empty strings.");
	if(typeof command !== "function")
		throw new TypeError("'command' should be a function.");

	for(const name of names)
		cmds[name.toLowerCase()] = command;

	const [main, ...aliases] = names;
	command.mainName = main;
	cmdsInfos.set(main, { aliases, params, description });
}


const logName = /[0-9]{4}(-[0-9]{2}){2} ([0-9]{2}:){2}[0-9]{2}(.log)?$/;

require(".").register(["logs", "log"],
"Shows help about the logs.",
(msg, [cmd, ...prm]) => {
	const { channel } = msg;
	const command = cmd?.toLowerCase();
	prm = prm.join(" ");

	if(!command || command === "help")
		channel.send(HELP_EMBED);
	else if(cmds[command])
		cmds[command](msg, prm, cmd);
	else // Trying to get a log or log list
	{
		let specificOne = false;
		if(prm)
			cmd += " " + prm;

		if(logName.test(cmd))
		{
			if(!cmd.endsWith(".log"))
				cmd += ".log";
			
			specificOne = cmd.slice(-23);
			cmd = cmd.slice(0, cmd.length - 24);
		}

		if(!lambs.includes(cmd))
			cmd = cmd.replaceAll(" ", "");
		const base = root + "/logs/" + cmd;

		if(specificOne)
		{
			if(fs.existsSync(`${base}/${specificOne}`))
				sendLog(channel, base, specificOne);
			else
				channel.send("This log file does not exist.");
		}
		else if(!fs.existsSync(base))
			channel.send(`The lamb ${cmd} does not exist or has no logs.`);
		else
		{
			fs.readdir(base, (err, files) => {
				if(err)
					error(err);
				else if(!files.length)
					channel.send("This lamb does not have any logs.");
				else
				{
					// With the name pattern of the logs, we can cram 83 of them into a message, including the line breaks.
					const nFiles = files.length;
					channel.send(`There ${nFiles === 1 ? "is" : "are"} ${files.length} log${nFiles === 1 ? "" : "s"} for this lamb${nFiles > 50 ? `.\nHere are the 50 last:\n${files.slice(-50).join("\n")}`
									  : `:\n${files.join("\n")}`
					}`);
				}
			});
		}
	}
}, {master: true});


register("list",
"Lists how many logs each lamb has and which the last one is.",
({channel}) => {
	const logsFolder = `${root}/logs/`;
	const logList = lambs.map(lamb => {
		if(!fs.existsSync(logsFolder+lamb))
			return `${lamb}: no logs`;
		
		const files = fs.readdirSync(logsFolder+lamb);
		return files.length
			? `${lamb}: ${files.length} logs (last one: ${files.pop()})`
			: `${lamb}: no logs`;
	});
	channel.send(logList.join("\n") || "No logs.");
})


register(["last", "lastOne"], "<lamb>",
"Show the last log for that lamb.",
({channel}, lamb) => {
	if(!lambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");
	const logsFolder = `${root}/logs/${lamb}`;
	if(!fs.existsSync(logsFolder))
		channel.send(`The lamb ${lamb} does not exist or has no logs.`);
	else
	{
		fs.readdir(logsFolder, (err, files) => {
			if(err)
				error(err);
			else if(!files.length)
				channel.send("This lamb does not have any logs.");
			else
				sendLog(channel, logsFolder, files.pop());
		});
	}
});


register("all", "<lamb>",
"Gets all the logs of a lamb. Add `-dl` to get them as files instead of messages (Commander Shepherd will try to zip them).",
(message, lamb) => {
	const { channel } = message;

	let dl = false;
	if(lamb.startsWith("-dl")) { lamb = lamb.substring(4); dl = true; }
	else if(lamb.endsWith("-dl")) { lamb = lamb.substring(0, lamb.length - 4); dl = true; }

	if(!lambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");
	const logsFolder = `${root}/logs/${lamb}`;

	if(!fs.existsSync(logsFolder))
		return channel.send(`The lamb ${lamb} does not exist or has no logs.`);

	fs.readdir(logsFolder, async (err, files) => {
		if(err)
			return error(err);
		if(!files.length)
			return channel.send("This lamb does not have any logs.");

		if(dl)
		{
			message.react("⚙️").catch(Function());

			const zip = new JSZip();
			for(const log of files)
				zip.file(log, fs.readFileSync(`${logsFolder}/${log}`, "utf-8"));

			channel.send({ files: [{attachment: await zip.generateAsync({
				type: "nodebuffer", streamFiles:true,
				compression: "DEFLATE", compressionOptions: { level: 6 },
			}), name: `${lamb}.zip`}] }).catch(err => {
				if(err.httpStatus === 413) // Request entity too large
					channel.send("There are too many logs. Even zipped, they exceed to upload size limit.");
				else if(err.message.includes("abort"))
					channel.send("There are way too many logs, I can’t even complete the request to upload the zip!").catch(Function());
				else
					error(err);
			});
		}
		else for(const log of files)
			await Promise.all(sendLog(channel, logsFolder, log));
	});
});


register(["deleteOlderThan", "removeOlderThan", "eraseOlderThan"], "<date> <lamb>",
"Deletes all logs for that lamb older than the given date. The date should have the format `yyyy[-mm[-dd]]`.",
({channel}, prm) => {
	const cmd = prm.split(" ", 1)[0].toLowerCase();
	let lamb = prm.length > (cmd.length + 1) ? prm.slice(cmd.length + 1) : "";
	if(!lambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");
	const logsFolder = `${root}/logs/${lamb}`;

	if(!fs.existsSync(logsFolder))
		channel.send(`The lamb ${lamb} does not exist or has no logs.`);
	else
	{
		fs.readdir(logsFolder, (err, files) => {
			if(err)
				error(err);
			else if(!files.length)
				channel.send("This lamb does not have any logs.");
			else
			{
				let n = 0, errs = 0;
				for(const file of files)
				{
					if(file < cmd)
					{
						try { fs.unlinkSync(`${logsFolder}/${file}`); n++; }
						catch(err) { error(err); errs++; }
					}
					else
						break;
				}

				channel.send(`Deleted ${n} log${n === 1 ? "" : "s"}. ${errs} error${errs === 1 ? "" : "s"}.`);
			}
		});
	}
});


register(["deleteAll", "removeAll", "eraseAll", "clean", "clear", "clearAll"], "<lamb>",
"Deletes all logs for that lamb. Also work with `|logs clear <lamb>`.",
({channel}, lamb) => {
	if(!lambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");
	const logsFolder = `${root}/logs/${lamb}`;
	if(!fs.existsSync(logsFolder))
		channel.send(`The lamb ${lamb} does not exist or has no logs.`);
	else
	{
		fs.readdir(logsFolder, (err, files) => {
			if(err)
				error(err);
			else if(!files.length)
				channel.send("This lamb does not have any logs.");
			else
			{
				let n = 0, errs = 0;
				for(const file of files)
				{
					try { fs.unlinkSync(`${logsFolder}/${file}`); n++; }
					catch(err) { error(err); errs++; }
				}

				channel.send(`Deleted ${n} log${n === 1 ? "" : "s"}. ${errs} error${errs === 1 ? "" : "s"}.`);
			}
		});
	}
});


const HELP_EMBED = { embeds: [{
	color: 0xBBFFBB,
	title: "Log commands",
	description: "——————————————————————————————————",
	fields: [
		{ name: "|logs <lamb>", value: "Show the log names for that lamb. If there are more than 50, only the last 50 are shown." },
		{ name: "|log <lamb> <log>", value: "Get a specific log file. Example: `|log My awesome lamb 2019-05-24 13:54:02`." },
		...cmdsInfos.map(({aliases, params, description}, name) => {
			return {
				name: `|logs ${name}${params ? " "+params : ""}`,
				value: (aliases.length ? `*Alias : |${aliases.join(", |")}*\n` : "") + description,
			};
		})
	]
}]};
