# Commander Shepherd

## Presentation

Such a bold nickname, innit.

Commander Shepherd is a bot intending to manage your other bots. It will automatically restart them if they crash, while detectint a crash on boot and avoid a loop. It can update them with `git pull` and start, restart and stop them at will.

It can actually manage any Node.JS app, not just bots. These are referred to as "lambs".

If you shut the Shepherd down, it will also shut its children down, sparing you a bunch of manual `|stop` commands. But, if the Shepherd crashes, it won't take them with it. However, it means your lambs will become "ghost" processes, and you will have to shut them down with commands (`ps aux | grep node` and `kill <pid>`, stuff like that) before you restart the Shepherd... But it's pretty stable, so you should not be bothered.

With this, if you have several bots that you host yourself, you will only have one command to type to start them all.

The Shepherd must to be on at least one server where you are too, or you will not be able to send messages to it.


## Use

To use it, you first have to create an `auth.json` file in the same folder as `bot.js` and give it the following data:

```JSON
{
	"token": "the commander's identification token",
	"master": "your own user id",
	"root": "the absolute path to the directory containing all your bots, for instance ~/NodeApps",
	"lambs": ["directory of first lamb", "directory of second lamb", "etc"]
}
```

To update the lambs, Commander Shepherd simply makes a `git pull` in their directory, so you should make sure the local git repository is initialised and connected to the remote one.

Aaaand... Done! You should then execute `./launch.sh`. It will start the ssh-agent, prompt you for your SSH key, update the Shepherd (with a `git pull`) then start it. It only works on UNIX systems, though.

Arguments:
- `--dont-update`: Launch the Shepherd without updating it. Alias: `-du`
- `--engine=<engine>`: set the JS engine. Example: `--engine=bun`. Default engine is `node`. Alias: `-e`



## Licence

**Commander Shepherd** is published under the GNU General Public Licence v3 (GPL-3.0). For more details, see COPYING.txt, or this link: [https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)

![GPL](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/320px-GPLv3_Logo.svg.png)
