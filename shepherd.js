"use strict";

var master, lambs = {};
const engine = process.env.engine || "node";
const installCmd = engine === "bun" ? "bun install" : "npm i";
const ciCmd = engine === "bun" ? "bun install --frozen-lockfile" : "npm ci";

module.exports = exports = {
	addLamb, removeLamb,
	placeFile,
	startLamb,
	stopLamb,
	restartLamb,
	updateLamb,
	lambs,
};

const { exec, spawn } = require("node:child_process");
const {
	existsSync, readdirSync,
	mkdirSync, writeFile, rm,
	createWriteStream,
} = require("node:fs");
const https = require("https");

const settings = require("./settings");
const {
	onMasterReady,
	auth,
	registerCleanup,
} = require("./discord");
const { splitMessage } = require("./utils/splitMessage");
const { root, lambs: regLambs } = auth;
const error = require("./utils/error");

onMasterReady(m => {
	master = m;
	if(regLambs) for(const lamb of regLambs)
		updateLamb(lamb);
});

registerCleanup(() => {
	for(const lamb in lambs)
		if(lambs[lamb].process)
			stopLamb(lamb);
});


class ChildLambError extends Error {
	constructor(lamb, message) {
		super(message);
		this.name = "ChildLambError";
		this.lamb = lamb;
	}
}


const crashForgiveness = setInterval(() => {
	for(const lamb of Object.values(lambs))
		if(lamb.process && lamb.restartTries > 0)
			lamb.restartTries--;
}, 600000); // 10 minutes



function addLamb(name, git, channel = master)
{
	function addToAuth(onSuccess) {
		regLambs.push(name);
		writeFile("auth.json", JSON.stringify(auth, null, "\t"), err => {
			if(err)
				regLambs.pop();
			channel.send(err || onSuccess);
		});
	}

	if(regLambs.includes(name))
		return channel.send("There already is a lamb with that name.");

	const path = `${root}/${name}`;
	if(existsSync(path) && readdirSync(path).length)
		addToAuth(name+" was added to `auth.json`, but its repo was not cloned as there already was a non-empty folder with that name.");
	else
	{
		exec(`git clone ${git} "${path}"`, (error, _, stderr) => {
			if(error)
				splitMessage(error + stderr).forEach(channel.send.bind(channel));
			else
				exec(`cd "${path}" && ${ciCmd}`, (err) =>
					addToAuth(err ? `Lamb added, but the \`${ci}\` failed.` : "Lamb added, ready to start!")
				);
		});
	}
}

function removeLamb(lamb, deleteFolder = false, channel = master)
{
	if(!regLambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");

	const pos = regLambs.indexOf(lamb);
	if(pos === -1)
		return channel.send("There is no lamb with that name.");

	regLambs.splice(pos, 1);
	writeFile("auth.json", JSON.stringify(auth, null, "\t"), err => {
		if(err)
		{
			regLambs.push(lamb);
			channel.send(err);
		}
		else if(deleteFolder)
		{
			rm(`${root}/${lamb}`, {force: true, recursive: true}, err => {
				if(err) regLambs.push(lamb);
				channel.send(err || "Lamb removed and deleted.");
			});
		}
		else
			channel.send("Lamb removed.");
	});
}

function placeFile(lamb, file, channel = master)
{
	if(!existsSync(`${root}/${lamb}`))
		lamb = lamb.replaceAll(" ", "");

	const path = `${root}/${lamb}`;
	if(!existsSync(path))
		throw new Error(`Error: could not find lamb '${lamb}'.`);

	https.get(file.url, res => {
		if(res.statusCode !== 200)
		{
			res.resume();
			return channel.send(`Error: got status ${res.statusCode} while trying to retrieve the file.`);
		}

		const stream = createWriteStream(`${path}/${file.name}`);
		res.pipe(stream);
		stream.on("error", error);
		stream.on("finish", () => {
		    stream.close();
		    channel.send("File placed.");
		});
	}).on("error", error);
}



function startLamb(lamb, channel = master)
{
	if(!regLambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");

	const path = `${root}/${lamb}`;
	const logsPath = `${root}/logs/${lamb}`;

	if(!existsSync(path))
		throw new Error(`Error: could not find lamb '${lamb}'.`);

	if(!existsSync(logsPath))
		mkdirSync(logsPath);

	if(!lambs[lamb])
		lambs[lamb] = {
			process: null,
			restartTries: 0, // Number of retries in case of crash; stops strying at 3
		};

	const child = lambs[lamb];

	if(child.process)
		return channel.send(`${lamb} is already running.`);

	console.log(`Starting ${lamb}...`);

	const bot = spawn(engine, [`"${path}"`], { cwd: path, shell: true, stdio: ["inherit", "pipe", "pipe"] });
	child.process = bot;

	bot.once("spawn", () => channel.send(`${lamb} started.`));

	bot.on("error", err => {
		const except = new ChildLambError(lamb, err.message);
		except.actualException = err;
		error(except);
		child.process = null;
	});

	bot.stdout.on("data", data => console.log(`\n[${lamb}]\n${data}`));
	bot.stderr.on("data", data => {
		console.error(`\n[${lamb}]\n${data}`);
		writeFile(`${logsPath}/${new Date().format()}.log`, data, err => { if(err) error(err); });
	});

	bot.on("exit", code => {
		child.process = null;
		if(code)
		{
			const retry = child.restartTries < 3;
			error(new ChildLambError(lamb, `${lamb} ended with code ${code}. ${retry ? "I will try to restart it." : "It just keeps crashing, I give up."}`));

			if(retry)
			{
				child.restartTries++;
				startLamb(lamb);
			}
		}
	});
}


function stopLamb(lamb, restart, channel = master)
{
	if(!(lamb in lambs))
		lamb = lamb.replaceAll(" ", "");

	const child = lambs[lamb];

	if(child?.process)
	{
		child.process.kill("SIGKILL"); // Because the lambs have to be launched in a shell, we have two processes to kill
		exec(`ps aux | grep "${lamb}"`, (error, stdout, stderr) => {
			if(error)
				return channel.send(error + stderr);

			exec("kill " + stdout.match(/[0-9]+/), (error, stdout, stderr) => {
				if(!stderr && !error)
				{
					child.process = null;
					channel.send(`${lamb} has been stopped.`);

					if(restart)
						startLamb(lamb);
				}
				else
				{
					if(stderr)
						channel.send(`stderr: ${stderr}${error ? "\n(see also error in the shell)" : ""}`);

					if(error !== null)
						console.error(error);
				}
			});
		});
	}
	else
		channel.send(`I cannot stop ${lamb}, it is not running.`);
}

function restartLamb(lamb, channel = master)
{
	if(lambs[lamb]?.process)
		stopLamb(lamb, true, channel);
	else
	{
		try { startLamb(lamb, channel); }
		catch({message}) { channel.send(message); }
	}
}


function updateLamb(lamb, force = false, ci = false, channel = master)
{
	if(!regLambs.includes(lamb))
		lamb = lamb.replaceAll(" ", "");

	const path = `${root}/${lamb}`;

	if(!existsSync(path))
		throw new Error(`Error: could not find lamb '${lamb}'.`);

	channel.send(`Updating ${lamb}...`);
	const cmd = `cd "${path}" && ${
		force
			? "git checkout master && git fetch --all -f && git reset --hard origin/master"
			: "git pull"} && ${
		ci ? ciCmd : installCmd}`;

	exec(cmd, (err, stdout, stderr) => {
		if(err !== null)
		{
			channel.send(`Update error: ${stderr}${err ? `\nUpdating ${lamb} failed (see the error in the console).` : ""}`);
			console.error(err);
		}
		else
		{
			channel.send(`${lamb} update successful${settings.passPullInfo ? `: ${stdout}` : "."}`);
			if(lambs[lamb])
				lambs[lamb].restartTries = 0;
			restartLamb(lamb);
		}
	});
}
