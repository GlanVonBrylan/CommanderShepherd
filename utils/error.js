"use strict";

module.exports = exports = err => {
	let msg = "An error occurred; read the console for details.";

	if(err.message)
	{
		if(err.name === "DiscordAPIError")
			msg += `\nMessage: ${err.message}\nPath: ${err.path}`;
		else if(err.name === "ChildLambError")
			msg += `\nError with ${err.lamb}. Message: ${err.message}`;
		else
			msg += `\nMessage: ${err.message}`;
	}

	require("../discord").master?.send(msg).catch(console.error);
	console.error(err);
}

process.on("unhandledRejection", exports);