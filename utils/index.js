"use strict";


String.prototype.replaceAt = function(index, replacement) {
	if(index < 0)
		index = this.length + index;
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}


Array.prototype.rand = function() {
	return this[Math.floor(Math.random() * this.length)];
}


function pad(n) {
	return n < 10 ? "0"+n : n;
}

Date.prototype.format = function() {
	return `${this.getFullYear()}-${pad(this.getMonth() + 1)}-${pad(this.getDate())} ${pad(this.getHours())}:${pad(this.getMinutes())}:${pad(this.getSeconds())}`;
}

exports.pad = pad;

for(const file of require("node:fs").readdirSync(__dirname))
	if(file !== "index.js")
		exports[file.slice(0, -3)] = require("./"+file);
