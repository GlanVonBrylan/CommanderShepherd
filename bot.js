"use strict";

const { exec, spawn } = require("node:child_process");

require("./utils");
const commands = require("./commands");
const { register } = commands;
const error = require("./utils/error");
const {
	client,
	registerCleanup,
	exit,
} = require("./discord");


client.on("messageCreate", msg => {
	const { content } = msg;
	if(!content || msg.author.bot || msg.webhookID || content[0] !== "|" || content[1] === "|") // The last one is to leave spoiler tags alone
		return;

	const command = content.split(" ", 1)[0].substring(1);
	const params = content.length === command.length + 1
		? ""
		: content.substring(command.length + 2).trim().split(" ");

	if(commands(command, msg, params.length > 1 || params[0] ? params : []) === null)
		msg.channel.send("Unknown command. Try `|help`.").then(msg => setTimeout(() => msg.delete().catch(Function()), 2000));
});



const masterOnly = true, inline = true;
const { restartLamb } = require("./shepherd");

register("shutdown",
"Shuts the Shepherd and all its children down.",
exit, {masterOnly, inline});

register("restart",
"Restarts the Shepherd and all its children, and updates the Sepherd in the process.\nAdd `-ci to also update its packages to match the lockfile.`",
(_, prm) => {
	if(prm.length && (prm.length > 1 || prm[0] !== "-ci"))
		restartLamb(prm.join(" "));
	else
	{
		const { engine = "node" } = process.env;
		const update = engine === "bun" ? "bun install" : "npm ci";
		registerCleanup(() => {
			exec(prm[0] === "-ci" ? `git pull && ${update}`: "git pull", (err, stdout, stderr) => {
				if(err !== null)
					error(new Error("Could not update myself:\n" + stderr));

				spawn(engine, ["."], { cwd: ".", shell: true, stdio: "inherit", detached: true })
					.unref();
			});
		});
		exit();
	}
}, {masterOnly, inline});
